# Downloading images produced by the Automotive SIG

The Automotive SIG provides OSBuild manifests and instructions to build images
for an AArch64 Virtual Machine and a Raspberry Pi 4.

We are also working on a process to automatically build and test these images
as we update them and as CentOS Stream changes. This would help us continuously
deliver working images. However, this is a work in progress. We cannot currently point you to existing
and downloadable images. If you would like to check our images, follow
the instructions on the [Building Images](https://sigs.centos.org/automotive/building/) page.
